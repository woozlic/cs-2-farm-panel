import sys

from mainwindow import *
from pyuac import main_requires_admin


@main_requires_admin
def main():
    OnStart()
    os.system('CLS')
    app = QtWidgets.QApplication(sys.argv)
    app.setWindowIcon(QtGui.QIcon('img/icon.png'))

    MainWindow = QtWidgets.QMainWindow()
    MainWindow.setWindowIcon(QtGui.QIcon('img/icon.png'))

    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)

    MainWindow.show()
    time.sleep(5)
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()